import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:requests/requests.dart';

import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';

class LoginPage extends StatefulWidget {

  LoginPage({this.onSignedIn});

  final Function(int) onSignedIn;

  @override
  State<StatefulWidget> createState() => new _LoginState();
}

const loginUrl = "http://192.168.31.103:8000/users/login";
const registrationUrl = "http://192.168.31.103:8000/users/add";

enum SubmitAction {
  login,
  register
}

class _LoginState extends State<LoginPage> {

  final formKey = new GlobalKey<FormState>();

  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  String _username;
  String _password;

  String _status = " ";

  void validateAndSubmit(SubmitAction action) async {
    final form = formKey.currentState;
    if (form.validate() == true) {

      _username = _usernameController.text;
      _password = _passwordController.text;

      var response;

      switch (action) {
        case SubmitAction.login: {
          response = await http.post(
              loginUrl,
              headers: <String, String> {'Content-Type': 'application/json'},
              body: jsonEncode( {'username': _username, 'password': _password} )
          );
          break;
        }
        case SubmitAction.register: {
          response = await http.post(
              registrationUrl,
              headers: <String, String> {'Content-Type': 'application/json'},
              body: jsonEncode( {'id': 0, 'username': _username, 'password': _password} )
          );
          break;
        }
      }

      if ((action == SubmitAction.login && (response.body != "There is no user with this username." && response.body != "Wrong password." && response.body != "Unknown error."))
        || (action == SubmitAction.register && response.body != "Error creating new user")) {

        var dio = Dio();
        Directory appDocDir = await getApplicationDocumentsDirectory();
        String appDocPath = appDocDir.path;
        var cookieJar = PersistCookieJar(dir: appDocPath + '/./cookies/');
        dio.interceptors.add(CookieManager(cookieJar));

        List<Cookie> cookies;
        cookieJar.loadForRequest(Uri.parse('http://192.168.31.103:8000/users/get_cookie'));
        await dio.post("http://192.168.31.103:8000/users/get_cookie", data: {"id": "${int.parse(response.body)}"});

        widget.onSignedIn(int.parse(response.body));
      }

      setState(() {
        _status = response.body;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      home: new Scaffold(
      appBar: new AppBar(
        title: new Text('Login Page'),
      ),
        body: new Container(
          padding: EdgeInsets.all(16.0),
          child: new Form(
            key: formKey,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                new TextFormField(
                  decoration: new InputDecoration(labelText: "Username"),
                  validator: (value) => value.isEmpty ? '*This field cannot be empty.' : null,
                  controller: _usernameController,
                ),
                new TextFormField(
                  decoration: new InputDecoration(labelText: "Password"),
                  obscureText: true,
                  validator: (value) => value.isEmpty ? '*This field cannot be empty.' : null,
                  controller: _passwordController,
                ),
                new RaisedButton(
                  child: new Text('Sign in', style: new TextStyle(fontSize: 20.0)),
                  onPressed: () => validateAndSubmit(SubmitAction.login),
                ),
                new FlatButton(
                  child: new Text('Create a new account', style: TextStyle(fontSize: 20.0)),
                  onPressed: () => validateAndSubmit(SubmitAction.register),
                ),
                new Text(_status, style: TextStyle(fontSize: 18.0)),
              ],
            ),
          )
        )
      ),
    );
  }
}