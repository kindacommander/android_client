import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:audioplayer/audioplayer.dart';
import 'package:http/http.dart' as http;

import 'dart:io';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';

const streamUrl = "http://192.168.31.103:8000/track/";
const infoUrl = "http://192.168.31.103:8000/track/info/";
const usersUrl = "http://192.168.31.103:8000/users/";

class TrackDetails {
  final int id;
  final String name;
  final String author;
  final String album;
  final String genre;
  final String lyrics;
  bool isLiked;
  final String image;

  TrackDetails({this.id, this.name, this.author, this.album, this.genre, this.lyrics, this.isLiked, this.image});

  factory TrackDetails.fromJson(Map<String, dynamic> json) {
    return TrackDetails(
      id: json['id'],
      name: json['name'] as String,
      author: json['author'] as String,
      album: json['album'] as String,
      genre: json['genre'] as String,
      lyrics: json['lyrics'] as String,
      isLiked: json['is_liked'] as bool,
      image: json['image_path'] as String,
    );
  }
}

class StreamingPage extends StatefulWidget {

  int currUserId;
  int currTrackId;

  TrackDetails currTrackDetails;

  PlayerState currPlayerState;

  VoidCallback onLeaveTrackDetailsPage;
  VoidCallback onPause;
  VoidCallback onPlay;
  VoidCallback onNextTrackSelection;
  VoidCallback onPrevTrackSelection;
  VoidCallback onLikedStatusUpdate;

  StreamingPage({
    this.currUserId,
    this.currTrackId,
    this.onLeaveTrackDetailsPage,
    this.onLikedStatusUpdate,
    this.currPlayerState,
    this.currTrackDetails,
    this.onPause,
    this.onPlay,
    this.onNextTrackSelection,
    this.onPrevTrackSelection
  });

  @override
  _StreamingState createState() => _StreamingState();
}

int fromJson(Map<String, dynamic> json) {
  return json['num'];
}

Future<int> getNumberOfTracks() async {
  final res = await http.get('http://192.168.31.103:8000/number/of/tracks');
  return fromJson(json.decode(res.body));
}

enum PlayerState {
  playing,
  paused,
  stopped
}

enum PlayerAction{
  play,
  pause,
  stop
}

class _StreamingState extends State<StreamingPage> {
  @override
  void initState() {
    super.initState();
  }

  Duration trackDuration;
  Duration currPosition;

  AudioPlayer audioPlayer;

  PlayerState playerState = PlayerState.stopped;

  get durationText =>
      trackDuration != null ? trackDuration.toString().split('.').first : '';

  get positionText =>
      currPosition != null ? currPosition.toString().split('.').first : '';

  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  bool isPaused = true;
  bool isStarted = false;
  //castyl
  bool isInit = true;

  var _fetchTimer;

  static int numOfTracks = 2;

  /*
  void initAudioPlayer() {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged.listen((p) => setState(() => currPosition = p));
    _audioPlayerStateSubscription = audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => trackDuration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          currPosition = trackDuration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        trackDuration = Duration(seconds: 0);
        currPosition = Duration(seconds: 0);
      });
    });
  }
  */

  void updateTrackLikedStatus(int userId, int trackId) async {
    var res = await http.post(
        "http://192.168.31.103:8000/track/like/" + widget.currTrackDetails.isLiked.toString(),
        headers: <String, String> {'Content-Type': 'application/json'},
        body: jsonEncode( {'user_id': userId, 'track_id': trackId} )
    );
  }

  /*
  setUpTimedFetch() {
    return new Timer.periodic(Duration(seconds: 10), (timer) async {
      int newValue = await getNumberOfTracks();
      setState(() {
        numOfTracks = newValue;
      });
    });
  }
   */

  /*
  void _fetchTrackInfo() async {
    var res = await http.get('http://192.168.31.103:8000/track/info/${widget.currTrackId}/user/${widget.currUserId}');
    final resBody = json.decode(res.body);
    setState(() {
      _currTrackInfo = TrackDetails.fromJson(resBody);
    });
  }
  */

  /*
  Future callAudio(PlayerAction action) async {
    if (action == PlayerAction.play) {
      await audioPlayer.play(streamUrl + widget.currTrackId.toString());
      setState(() => playerState = PlayerState.playing);
    }
    else if (action == PlayerAction.pause) {
      await audioPlayer.pause();
      setState(() => playerState = PlayerState.paused);
    }
    else if (action == PlayerAction.stop) {
      await audioPlayer.stop();
      setState(() {
        playerState = PlayerState.stopped;
        currPosition = Duration();
      });
    }
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }
   */

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Builder(
            builder: (BuildContext context) {
              return IconButton(
                icon: const Icon(Icons.arrow_drop_down),
                iconSize: 38,
                color: Colors.white,
                onPressed: (){
                  widget.onLeaveTrackDetailsPage();
                },
              );
            }
          ),
          title: Text("User#${widget.currUserId}"),
        ),
        body: Center(
          child: Column(
            children: <Widget> [
              Container(
                height: 300,
                width: 300,
                child: FittedBox(
                  fit: BoxFit.fill,
                  child: Image.memory(base64Decode(widget.currTrackDetails.image), gaplessPlayback: true),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Column(
                  children: <Widget> [
                    Text (
                      widget.currTrackDetails.album.toString(),
                      style: TextStyle(fontSize: 20),
                    ),
                    Row (
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget> [
                        Expanded(
                          child: Text(
                            widget.currTrackDetails.author.toString(),
                            style: TextStyle(fontSize: 18),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            widget.currTrackDetails.name.toString(),
                            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              _buildAudioPlayer(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildAudioPlayer() => Container(
    padding: EdgeInsets.all(16.0),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        /*
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget> [
              Text(
                  positionText
              ),
              Slider(
                  value: currPosition?.inMilliseconds?.toDouble() ?? 0.0,
                  onChanged: (double value) {
                    return audioPlayer.seek((value / 1000).roundToDouble());
                  },
                  min: 0.0,
                  max: trackDuration.inMilliseconds.toDouble()
              ),
              Text(
                  durationText
              ),
            ],
        ),
        */
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget> [
            IconButton(
              icon: Icon(Icons.refresh),
              iconSize: 42,
              color: Colors.black,
              onPressed: () {
                //TODO repeat track
              },
            ),
            Ink (
              decoration: ShapeDecoration(
                color: Colors.blue,
                shape: CircleBorder(),
              ),
              child: IconButton(
                icon: Icon(Icons.skip_previous),
                iconSize: 44,
                color: Colors.white,
                onPressed: () {
                  widget.onPrevTrackSelection();
                },
              ),
            ),
            Ink(
              decoration: ShapeDecoration(
                color: Colors.blue,
                shape: CircleBorder(),
              ),
              child: IconButton(
                icon: (widget.currPlayerState == PlayerState.paused)? Icon(Icons.play_arrow): Icon(Icons.pause),
                iconSize: 66,
                color: Colors.white,
                onPressed: () {
                  (widget.currPlayerState == PlayerState.paused)? widget.onPlay(): widget.onPause();
                },
              ),
            ),
            Ink(
                decoration: ShapeDecoration(
                  color: Colors.blue,
                  shape: CircleBorder(),
                ),
                child: IconButton(
                  icon: Icon(Icons.skip_next),
                  iconSize: 44,
                  color: Colors.white,
                  onPressed: () {
                    widget.onNextTrackSelection();
                  },
                )
            ),
            IconButton(
              icon: widget.currTrackDetails.isLiked? Icon(Icons.favorite): Icon(Icons.favorite_border),
              iconSize: 42,
              color: widget.currTrackDetails.isLiked? Colors.red: Colors.black,
              onPressed: () {
                updateTrackLikedStatus(widget.currUserId, widget.currTrackDetails.id);
                setState(() {
                  widget.currTrackDetails.isLiked = !widget.currTrackDetails.isLiked;
                });
              },
            ),
          ],
        ),
      ],
    ),
  );
}