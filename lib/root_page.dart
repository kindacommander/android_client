import 'package:flutter/material.dart';
import 'login_page.dart';
import 'streaming_page.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'dart:io';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'package:audioplayer/audioplayer.dart';

const checkCookieUrl = "http://192.168.31.103:8000/users/check_cookie";
const streamUrl = "http://192.168.31.103:8000/track/";
const infoUrl = "http://192.168.31.103:8000/track/info/";
const usersUrl = "http://192.168.31.103:8000/users/";

class RootPage extends StatefulWidget {

  @override
    State<StatefulWidget> createState() => new _RootPageState();
}

enum LoginStatus {
  signedOut,
  signedIn
}

/*
class TrackCard {
  final id;
  final name;
  final author;
  final leadingImage;
  bool isLiked;

  TrackCard({this.id, this.name, this.author, this.leadingImage, this.isLiked});

  factory TrackCard.fromJson(Map<String, dynamic> json) {
    return TrackCard(
      id: json['id'],
      name: json['name'] as String,
      author: json['author'] as String,
      leadingImage: json['image_path'] as String,
      isLiked: json['is_liked'] as bool,
    );
  }
}
*/

/*
enum PlayerState {
  playing,
  paused,
  stopped
}
 */

enum PlayerAction {
  play,
  pause,
  stop
}

enum BottomBarState {
  enabled,
  disabled
}

enum DetailedPageState {
  enabled,
  disabled
}

class _RootPageState extends State<RootPage> {

  LoginStatus _loginStatus = LoginStatus.signedOut;

  List<TrackDetails> _trackList = [];

  int currUserId;
  int chosenTrackId;
  int chosenTrackIndex;

  //TODO in initState() fetch number of tracks
  int numberOfTracks = 2;

  int itemCount;

  AudioPlayer audioPlayer;

  PlayerState playerState = PlayerState.stopped;

  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  Duration trackDuration = Duration(milliseconds: 1);
  Duration currPosition = Duration(milliseconds: 1);

  BottomBarState bottomBar = BottomBarState.disabled;

  DetailedPageState detailedPage = DetailedPageState.disabled;

  void initAudioPlayer() {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged.listen((p) =>
        setState(() => currPosition = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
          if (s == AudioPlayerState.PLAYING) {
            setState(() => trackDuration = audioPlayer.duration);
          } else if (s == AudioPlayerState.STOPPED) {
            onComplete();
            setState(() {
              currPosition = trackDuration;
            });
          }
        }, onError: (msg) {
          setState(() {
            playerState = PlayerState.stopped;
            trackDuration = Duration(seconds: 0);
            currPosition = Duration(seconds: 0);
          });
        });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  void signOut() async {
    var dio = Dio();
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;
    var cookieJar = PersistCookieJar(dir: appDocPath + '/./cookies/');
    dio.interceptors.add(CookieManager(cookieJar));

    List<Cookie> cookies = [];
    cookies = cookieJar.loadForRequest(
        Uri.parse('http://192.168.31.103:8000/users/logout'));
    var res = await dio.post("http://192.168.31.103:8000/users/logout");
    print(res.data);

    await callAudio(PlayerAction.stop);

    setState(() {
      _loginStatus = LoginStatus.signedOut;
    });
  }

  // track list limit
  static int limit = 2;

  void fetchTrackList() async {
    var res = await http.post('http://192.168.31.103:8000/track/list',
        headers: <String, String>{'Content-Type': 'application/json'},
        body: jsonEncode({"user_id": currUserId, "limit": limit}));

    setState(() {
      _trackList = [];
      _trackList = new List<TrackDetails>.from(
          json.decode(res.body).map((i) => TrackDetails.fromJson(i)));
      itemCount = _trackList.length;
      print(res.body);
      print(_trackList);
    });
  }

  void checkAutoLogin() async {
    var dio = Dio();
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String appDocPath = appDocDir.path;
    var cookieJar = PersistCookieJar(dir: appDocPath + '/./cookies/');
    dio.interceptors.add(CookieManager(cookieJar));

    List<Cookie> cookies;
    cookies = cookieJar.loadForRequest(
        Uri.parse('http://192.168.31.103:8000/users/check_cookie'));
    var res = await dio.get("http://192.168.31.103:8000/users/check_cookie");
    print(res.data);
    if (res.data != "Cookie is not found") {
      setState(() {
        _loginStatus = LoginStatus.signedIn;
        currUserId = int.parse(res.data.substring(8));
      });
      fetchTrackList();
    }
  }

  Future callAudio(PlayerAction action) async {
    if (action == PlayerAction.play) {
      await audioPlayer.play(streamUrl + chosenTrackId.toString());
      setState(() => playerState = PlayerState.playing);
    }
    else if (action == PlayerAction.pause) {
      await audioPlayer.pause();
      setState(() => playerState = PlayerState.paused);
    }
    else if (action == PlayerAction.stop) {
      await audioPlayer.stop();
      setState(() {
        playerState = PlayerState.stopped;
        currPosition = Duration();
      });
    }
  }

  @override
  void initState() {
    super.initState();
    checkAutoLogin();
    _trackList = [];
    initAudioPlayer();
  }

  @override
  Widget build(BuildContext context) {
    switch (_loginStatus) {
      case LoginStatus.signedOut:
        return new LoginPage(onSignedIn: (int id) {
          setState(() {
            currUserId = id;
            _loginStatus = LoginStatus.signedIn;
          });
          fetchTrackList();
        });
      case LoginStatus.signedIn:
        switch (detailedPage) {
          case DetailedPageState.disabled:
            return _trackListView(context);
          case DetailedPageState.enabled:
            return StreamingPage(
                currUserId: currUserId,
                currTrackDetails: _trackList[chosenTrackIndex],
                currPlayerState: playerState,
                onPause: () {
                  callAudio(PlayerAction.pause);
                },
                onPlay: () {
                  callAudio(PlayerAction.play);
                },
                onNextTrackSelection: () {
                  callAudio(PlayerAction.stop);
                  setState(() {
                    if (chosenTrackId < numberOfTracks) {
                      chosenTrackIndex++;

                      chosenTrackId++;
                    }
                    else {
                      chosenTrackIndex = 0;

                      chosenTrackId = 1;
                    }
                  });
                  callAudio(PlayerAction.play);
                },
                onPrevTrackSelection: () {
                  callAudio(PlayerAction.stop);
                  setState(() {
                    if (numberOfTracks > 1) {
                      chosenTrackIndex--;

                      chosenTrackId--;
                    }
                    else {
                      chosenTrackId = numberOfTracks;

                      chosenTrackIndex = numberOfTracks - 1;
                    }
                  });
                  callAudio(PlayerAction.play);
                },
                onLeaveTrackDetailsPage: () {
                  fetchTrackList();
                  setState(() {
                    bottomBar = BottomBarState.enabled;
                    detailedPage = DetailedPageState.disabled;
                  });
                }
            );
        }
    }
  }

  void updateTrackLikedStatus(int userId, int trackId) async {
    var res = await http.post(
        "http://192.168.31.103:8000/track/like/" +
            _trackList[trackId].isLiked.toString(),
        headers: <String, String>{'Content-Type': 'application/json'},
        body: jsonEncode({'user_id': userId, 'track_id': trackId})
    );
  }

  Widget _trackListView(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.exit_to_app),
                  iconSize: 38,
                  color: Colors.white,
                  onPressed: signOut,
                );
              }
          ),
          title: const Text("Global Playlist"),
        ),
        body: Center(
          child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(8.0),
                  child: ListView.builder(
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemCount: _trackList?.length ?? 0,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(_trackList[index].name, style: TextStyle(
                            fontSize: 16.0, fontWeight: FontWeight.bold),),
                        subtitle: Text(_trackList[index].author,
                          style: TextStyle(fontSize: 14.0),),
                        trailing: Icon(
                            Icons.arrow_drop_down_circle, color: Colors.blue),
                        onTap: () {
                          callAudio(PlayerAction.stop);
                          setState(() {
                            chosenTrackId = _trackList[index].id;
                            chosenTrackIndex = index;
                            bottomBar = BottomBarState.enabled;
                          });
                          print(_trackList[chosenTrackIndex]);
                          callAudio(PlayerAction.play);
                        },
                      );
                    },
                  ),
                ),
                if (bottomBar == BottomBarState.enabled)
                  buildBottomBar(_trackList[chosenTrackIndex]),
              ]
          ),
        ),
      ),
    );
  }

  // backup commit

  double calculateWidthFactor() {
    double calculatedWidthFactor = (currPosition.inSeconds.toDouble() * 100 / trackDuration.inSeconds.toDouble() / 100);
    return (calculatedWidthFactor == null || calculatedWidthFactor < 0.0)? 0.0: calculatedWidthFactor;
  }

  Widget buildBottomBar(TrackDetails chosenTrack) {
    return Expanded(
        child: Align(
          alignment: FractionalOffset.bottomCenter,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget> [
              FractionallySizedBox(
                widthFactor: calculateWidthFactor(), //(currPosition.inSeconds.toDouble() * 100 / trackDuration.inSeconds.toDouble() / 100) ?? 0.0,
                child: Container(
                  alignment: FractionalOffset.topLeft,
                  height: 2.0,
                  color: Colors.blue,
                ),
              ),
              Container(
                  //padding: EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                  color: Colors.white70,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      GestureDetector(
                       child: Image.memory(
                           base64Decode(chosenTrack.image),
                           height: 85,
                           width: 85,
                           gaplessPlayback: true
                       ),
                        onTap: () {
                         setState(() {
                           detailedPage = DetailedPageState.enabled;
                         });
                        },
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            chosenTrack.name,
                            //textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(
                            chosenTrack.author,
                            //textAlign: TextAlign.left,
                            style: TextStyle(
                              fontSize: 14.0,
                            ),
                          )
                        ],
                      ),
                      ButtonBar(
                        buttonPadding: EdgeInsets.only(right: 10.0),
                        children: [
                          IconButton(
                            icon: chosenTrack.isLiked ? Icon(
                                Icons.favorite) : Icon(Icons.favorite_border),
                            color: chosenTrack.isLiked
                                ? Colors.red
                                : Colors.black,
                            iconSize: 30,
                            onPressed: () {
                              updateTrackLikedStatus(
                                  currUserId, chosenTrack.id);
                              setState(() {
                                chosenTrack.isLiked = !chosenTrack.isLiked;
                              });
                            },
                          ),
                          IconButton(
                            icon: (playerState == PlayerState.paused) ? Icon(
                                Icons.play_arrow) : Icon(Icons.pause),
                            color: Colors.blue,
                            iconSize: 35,
                            onPressed: () {
                              (playerState == PlayerState.paused) ? callAudio(
                                  PlayerAction.play) : callAudio(
                                  PlayerAction.pause);
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ),
    );
  }
}

/*
            child: ListTile(
              leading: Image.memory(base64Decode(_trackList[chosenTrackIndex].image), gaplessPlayback: true),
              title: Text(_trackList[chosenTrackIndex].name, style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),),
              subtitle: Text(_trackList[chosenTrackIndex].author, style: TextStyle(fontSize: 14.0),),
              trailing: Row(
                children: <Widget> [
                  IconButton(
                    icon: _trackList[chosenTrackIndex].isLiked? Icon(Icons.favorite): Icon(Icons.favorite_border),
                    color: _trackList[chosenTrackIndex].isLiked? Colors.red: Colors.black,
                    onPressed: (){
                      updateTrackLikedStatus(currUserId, _trackList[chosenTrackIndex].id);
                    },
                  ),
                  IconButton(
                    icon: (playerState == PlayerState.paused)? Icon(Icons.play_arrow): Icon(Icons.pause),
                    color: Colors.blue,
                    onPressed: (){
                      (playerState == PlayerState.paused)? callAudio(PlayerAction.play): callAudio(PlayerAction.pause);
                    },
                  ),
                ],
              ),
              onTap: () {
                setState(() {
                  bottomBar = BottomBarState.disabled;
                  detailedPage = DetailedPageState.enabled;
                });
              },
            ),
            */